# README (*Screenshots Below!*)#

Hanging Together is a multiplayer hangman app, played with two phones connected to the internet.

Front-end is React Native, back-end is a semi-authoritative server on NodeJS.  Websockets used for communication.

### TO-DO ###

* Host NodeJS server somewhere reliable
* Test Android app on different devices and fix problems
* Publish Android app to app store
* Build iOS app
* Add playing on one device feature
* Build web version with React

### How to set up ###

This repository has everything besides:

gradle.properties file in android/ should contain:

android.useDeprecatedNdk=true

MYAPP_RELEASE_STORE_FILE=your-app-key.keystore

MYAPP_RELEASE_KEY_ALIAS=your-app-alias

MYAPP_RELEASE_STORE_PASSWORD=your-store-password

MYAPP_RELEASE_KEY_PASSWORD=your-key-password

You will need to generate a key for debugging
and sign the package with it, to be able to install the apk on your phone.

A short guide for this:  http://stackoverflow.com/questions/35935060/how-can-i-generate-an-apk-that-can-run-without-server-with-react-native
For the last step, use app-release.apk rather than app-unsigned-release.apk

### Contact ###

Christian Safka

christiansafka@gmail.com

Tallinn, Estonia

### Screens ###

![HangingTogetherComplete.png](https://bitbucket.org/repo/6BoEXX/images/3404524806-HangingTogetherComplete.png)