'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  BackAndroid,
  Alert,
} from 'react-native';

const Hangman = require('./components/hangman');
const PickWord = require('./components/PickWord');

import Button from 'apsl-react-native-button'

class Hanging_With_Friends extends Component {

  constructor(props)
  {
    super(props);
    this.state = {
      text: "",
      gameStart: false,
      error: false,
    }
    this.roomFull = this.roomFull.bind(this);
  }

  componentWillMount() {
    this.bg = require('./images/wood_texture.jpg');
    this.title = require('./images/HangingWithFriends.png');
  }

  roomFull() {
    this.setState({gameStart: false, text: ""});
  }

  render() {

    let localState = false;

    const startGame = () => {
        this.setState({gameStart: true});
        localState = true;
    }
    const endGame = () => {
        this.setState({gameStart: false});
        localState = false;
    }

    const inputErrorCheck = () => {
        if(this.state.text != "")
        {
            this.setState({error: false});
            startGame();
        }
        else
        {
            this.setState({error: true});
        }
    }

    BackAndroid.addEventListener('hardwareBackPress', function() {
        if(localState == true)
        {
          Alert.alert(
              'Leave Room?',
              'This will end the current game',
              [
                {text: 'Cancel', onPress: () => console.log('cancelled leave room') },
                {text: 'OK', onPress: () => endGame()},
              ]
            );
          return true;
       }
       else{ return false; }
    });

    let errorShown = (this.state.error) ? <Text style={styles.error}>Enter a room key, and share it with your friend</Text> : <Text>''</Text>;

    let roomSelect = 
       <View style={styles.container}>
            <Image source={this.title}  style={styles.titleImage}></Image>
            <Image source={this.bg} style={styles.background}>
            <View>
              {errorShown}
            </View>
              <TextInput
                  style={styles.textInput}
                  onChangeText={(text) => this.setState({text})}
                  value={this.state.text}
                  placeholder="Enter a room key"
                  onEndEditing={this.clearFocus}
                  placeholderTextColor="white"
                  autoFocus={false}
                  underlineColorAndroid="#2980b9"
                  selectionColor="#3498db"
                  autoCorrect={false}
                />
              
            </Image>
            <Image source={this.bg} style={styles.background}>
              <View style={styles.rowCentering}>
                <Button style={styles.button} textStyle={styles.textStyle} onPress={inputErrorCheck}>Join/Create</Button>
              </View>
            </Image>
            <Image source={this.bg} style={styles.background}>
            </Image>
       </View>;

    let gameStarted =
      <Hangman roomFull={this.roomFull} roomID={this.state.text}></Hangman>;

    let isStarted = (this.state.gameStart) ? gameStarted : roomSelect;


    //the render {isStarted}
    return (
      <View>
        {isStarted}
      </View>
    );

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowCentering: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  error: {
    fontSize: 14,
    fontFamily: 'Avenir',
    color: '#ff0000',
  },
  button: {
     borderColor: '#2980b9',
     backgroundColor: '#3498db',
     width: 280,
     height: null,
  },
  titleImage: {
    resizeMode: 'cover',
    height: 180,
    width: 450,
  },
  background: {
    height: 180,
    width: 450,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    color: 'white',
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 30,
  },
  textInput: {
    fontSize: 27,
    height: 80, 
    width: 250,
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    color: 'white',
    borderColor: '#2980b9', 
    borderWidth: 2.8,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    marginBottom: 60,
    fontFamily: 'Avenir',
    borderColor: '#2980b9',
    borderWidth: 2,
  },
});


AppRegistry.registerComponent('Hanging_With_Friends', () => Hanging_With_Friends);

export default Hanging_With_Friends;