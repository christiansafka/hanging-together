'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Animated,
  Alert,
} from 'react-native';

import Button from 'apsl-react-native-button';

const HangmanView = require('./HangmanView');
const Word = require('./Word');

class GameOver extends Component 
{
	constructor(props)
	{
		super(props);
    this.state = {
      readyText: 'ready',
      fadeAnim: new Animated.Value(1), // init opacity 1
      buttonDisabled: false,
    }
    this.sendMessage = this.sendMessage.bind(this);
    this.fadeOut = this.fadeOut.bind(this);
	}

  componentDidMount() {
      this.fadeOut();
   }

  fadeOut() {
    Animated.timing(      
       this.state.fadeAnim,
        {
          toValue: 0,     
          duration: 1700,    
        },
     ).start();
  }

  sendMessage(action, data)
  {
    let resp = {
        action: action,
        data: data
    };

    try
    {
        this.props.ws.send(JSON.stringify(resp));
    }
    catch(e) { console.log('error sending ready: ' + e); }

       
  }

	render()
	{
		let winner = '';  //winning text
    let buttonDisabled = false;

    const image0 = require('../images/Hangman0.png');
    const image1 = require('../images/Hangman1.png');
    const image2 = require('../images/Hangman2.png');
    const image3 = require('../images/Hangman3.png');   //could probably do this dynamically
    const image4 = require('../images/Hangman4.png');
    const image5 = require('../images/Hangman5.png');
    const image6 = require('../images/Hangman6.png');
    const images = [image0, image1, image2, image3, image4, image5, image6];
    let currentImage = images[this.props.wrongGuesses];

    let randomText = '';
    let textArray = [];

    if(this.props.p == 2)
    {
      winner = (this.props.winner) ? 'You Escaped' : 'You Died';
      textArray = ["You have been hanged", "Better luck next ti-  oh"];
    }
    else 
    { 
      winner = (this.props.winner) ? 'Opponent Escaped' : 'Opponent Died';
      textArray = ["  Go pick up the pieces", "        He's dead, Jim", "  You are a wordsmith"];
    }

    let randomNumber = Math.floor(Math.random()*textArray.length);
    randomText = textArray[randomNumber];

    const escapedGif = <Image source={require('../images/runningman.gif')} style={{width:360,height:200,resizeMode:'cover',opacity:1}}  />
    let diedGif = <View>
                    <Text style={styles.randomText}>{randomText}</Text>
                    <Animated.View style={{opacity: this.state.fadeAnim}}>
                      <Animated.Image source={require('../images/deadmanslow.gif')}  style={{width:360, height:200, resizeMode:'cover'}} />
                    </Animated.View>
                  </View>;

    let gif = (this.props.winner) ? escapedGif : diedGif;

    let switchSides = () =>
    {
      this.sendMessage('switchSides', this.props.p);
      this.setState({readyText: 'waiting...', buttonDisabled: true});
    }

    let leaveRoom = () =>
    {
      Alert.alert(
          'Leave Room?',
          'Is it because you lost?',
          [
            {text: 'Cancel', onPress: () => console.log('cancelled leave room') },
            {text: 'OK', onPress: () => this.props.endGame},
          ]
        );
      return true;
    }

		return (
			<View style={styles.background}>
				<Text style={styles.text}>{winner}</Text>
        {gif}
				<Word word={this.props.word} />
        <View style={styles.rowCentering}>
				  <Button isDisabled={this.state.buttonDisabled} style={styles.readyBtn} textStyle={styles.textBtn} onPress={switchSides}>{this.state.readyText}</Button>
          <Button style={styles.leaveBtn} textStyle={styles.leaveBtntext} onPress={leaveRoom}>Leave Room</Button>
        </View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  background: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  leaveBtn: {
    borderColor: '#9b0000',
    backgroundColor: 'red',
    width: 160,
    height: 80,
    position: 'absolute',
    right: -67,
    top: 180,
  },
  leaveBtntext: {
    color: 'white',
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 22,
  },
  randomText: {
    position: 'absolute', 
    flex: 3,
    fontSize: 25,
    left: 50,
    top: 50,
    alignItems: 'center',
  },
  rowCentering: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  readyBtn: {
  	borderColor: '#2980b9',
    backgroundColor: '#3498db',
    width: 200,
    height: 60,
    marginTop: 60,
    marginBottom: 180,
  },
  text: {
  	fontFamily: 'Avenir',
  	color: '#724411',
  	fontSize: 37,
  	marginBottom: 0,
    marginTop: 20,
  },
  textBtn: {
    color: 'white',
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 32,
  },
});

module.exports = GameOver;
