'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
} from 'react-native';

class Waiting extends Component
{
	constructor(props)
	{
		super(props);
	}

	render()
	{
		return (
			<View style={styles.background}>
				  <Text style={styles.text}>{this.props.text}</Text>
          <Image source={require('../images/gears.gif')} />
			</View>
		);
	}
}

const styles = StyleSheet.create({
  background: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    marginTop: 65,
    fontSize: 28,
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 110,
  },
});

module.exports = Waiting;