'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
} from 'react-native';

import Button from 'apsl-react-native-button';

class PickWord extends Component 
{
	constructor(props)
	{
		super(props);
		this.state = {
			text: "",
			error: false,
		}
		this.makeMessage = this.makeMessage.bind(this);
		this.sendWord = this.sendWord.bind(this);
	}

	makeMessage(action, data)
	{
		let resp = {
            action: action,
            data: data
        };

        return JSON.stringify(resp);
       
	}

	sendWord()
	{
		console.log('sending word ' + word);
		try { this.props.ws.send(this.makeMessage('wordSelect', this.state.text)); }
		catch (e) { console.log('error sending wordSelect: ' + e); }
	}

	render()
	{
		const inputErrorCheck = () => {
	        if(/^[A-Za-z]{4,15}$/.test(this.state.text))
	        {
	            this.setState({error: false});
	            sendWord();
	        }
	        else
	        {
	            this.setState({error: true});
	        }
	    }

		const sendWord = () => {
			try { 
				this.props.ws.send(this.makeMessage('wordSelect', this.state.text)); 
				sentWord = sending;
				console.log('sent ' + this.state.text);
			}
			catch (e) { 
				console.log('error sending wordSelect: ' + e); 
			}
		}	

		let errorShown = (this.state.error) ? <Text style={styles.error}>Word must be 4 - 15 letters</Text> : <Text>''</Text>;

		let selecting = 
			<View style={styles.bg}>
				<Image style={styles.image} source={require('../images/tunnel.png')}>
					<TextInput
		              style={styles.textInput}
		              onChangeText={(text) => this.setState({text})}
		              value={this.state.text}
		              placeholder="Enter a word"
		              placeholderTextColor="white"
	                  autoFocus={true}
	                  underlineColorAndroid="#2980b9"
	                  selectionColor="#3498db"
	                  autoCorrect={false}
		            />
		            {errorShown}
		            <View style={styles.rowCentering}>
		            	<Button style={styles.button} textStyle={styles.textStyle} onPress={inputErrorCheck}>Choose Word</Button>
		            </View>
	            </Image>
	        </View>;

		return (
			<View style={styles.background}>
				{selecting}
				<Image source={require('../images/confusedman.png')} style={styles.bottomImage} />
			</View>
			
		);
	}
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  bottomImage: {
  	marginTop: 40,
  },
  error: {
  	color: 'red',
  	fontSize: 17,
  },
  bg: {
  	backgroundColor: '#724411',
  },
  rowCentering: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
  	flex: 1,
  	width: 362,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
  },
  textInput: {
  	marginTop: 125,
  	//borderColor: '#724411',
  	fontSize: 24,
    height: 50, 
    width: 200,
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    color: 'white',
    borderColor: '#2980b9', 
    borderWidth: 2.8,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  button: {
  	marginTop: 87,
 	borderColor: '#2980b9',
    backgroundColor: '#3498db',
    width: 234,
    height: null,
  },
  textStyle: {
  	color: 'white',
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 30,
  },
  buttonText: {
    flex: 1,
    height: 50,
    width: 100,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
    marginTop: 10,
  },
});

module.exports = PickWord;