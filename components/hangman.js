'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Alert,
} from 'react-native';


const PickWord = require('./PickWord');
const HangmanView = require('./HangmanView');
const GameOver = require('./GameOver');
const Waiting = require('./Waiting');


class hangman extends Component
{
	constructor(props)
	{
		super(props);
		let url = 'http://peaceful-reaches-54467.herokuapp.com/' + this.props.roomID;
		this.ws = new WebSocket(url);

		this.state = {
			gameState: 'connecting',
			wrongGuesses: 0,
			usedLetters: [],
			word: [],
			winner: false,
			p: 0, //player 1 or 2, used for GameOver, only to send to server to "switch sides"  ****UNNEEDED****
		}

		this.makeMessage = this.makeMessage.bind(this);

		this.ws.onopen = () => {
			try
			{
				//this.ws.send(this.makeMessage('bloop', 'bloop'));
			}
			catch(error)
			{
				console.log('ws not sending: ' + error);
			}
	   
	    };

	    this.ws.onmessage = (e) => {
	    	console.log(e.data);
	      	try
        	{
            	var msg = JSON.parse(e.data);
        	} 
        	catch (error)
       		{
       			console.log('couldnt parse msg');
            	this.ws.send(this.makeMessage('error', 'Invalid action'));
            	return;
        	}


       		try 
        	{
            	switch (msg.action) 
            	{
		      		case 'waiting': //waiting for players to connect
		      			this.setState({gameState: 'waiting'});
		      			break;
		      		case 'gameStart':
		      			this.setState({wrongGuesses: 0, gameState: (msg.data == 'chooseWord') ? 'chooseWord' : 'waitForWord'});
		      			break;
		      		case 'watching':
		      			this.setState({word: msg.data, gameState: 'watching'});
		      			break;
		      		case 'playing':
		      			this.setState({word: msg.data, gameState: 'playing'});
		      			break;
		      		case 'guess':
		      			break;    //for when guessing the full word is implemented
		      		case 'true':
		      			this.state.usedLetters.push(msg.data.letter);
		      			this.setState({word: msg.data.word, usedLetters: this.state.usedLetters});
		      			break;
		      		case 'false':  //if letter is not in word
		      			this.state.usedLetters.push(msg.data.letter);
		      			this.setState({usedLetters: this.state.usedLetters, wrongGuesses: msg.data.wrongGuesses});
		      			break;
		      		case 'gameOver':
		      			//this.state.usedLetters.push(msg.data.letter);
		      			let temp = (this.state.gameState == 'watching') ? 1 : 2;  //watching = word picker = player 1
		      			this.setState({gameState: 'gameOver', 
		      							p: temp, 
		      							winner: msg.data.winner, 
		      							word: msg.data.word, 
		      							usedLetters: [],
		      							endGame: this.props.roomFull});
		      			break;
		      		case 'roomFull':
		      			this.ws.close();
		      			Alert.alert(
			              'Room "' + msg.data + '" is full',
			              'This room already has 2 players',
			              [
			                {text: 'OK', onPress: this.props.roomFull},
			              ]
			            );
			            break;
		      		default:
		      			break;
	      		}
		    	 
	   		}
	   		catch (error) 
       		{
       			console.log('err from switch');
       			console.log(error);
            	this.ws.send(this.makeMessage('error', error.data));
        	}

	   	}

	    this.ws.onerror = (e) => {
	      // an error occurred
	      console.log('on error ws');
	      console.log(e);
	    };

	    this.ws.onclose = (e) => {
	      // connection closed
	      console.log('connection closed:');
	      console.log(e);
	      console.log(e.code, e.reason);
	    };

	    
	}

	componentWillUnmount()
	{
		this.ws.close();
	}

	makeMessage(action, data)
	{
		let resp = {
            action: action,
            data: data
          };

        return JSON.stringify(resp);
       
	}

	render()
	{
		let currentView = <Waiting text={'Connecting to server...'} />;
		switch(this.state.gameState)   //render view based on gamestate
		{
			case 'waiting':
				currentView = <Waiting text={'Waiting for second player'} />
				break;
			case 'chooseWord':
				currentView = <PickWord ws={this.ws} />;
				break;
			case 'waitForWord':
				currentView = <Waiting text={'Opponent choosing word'} />
				break;
			case 'watching':
				currentView = <HangmanView ws={this.ws} gameState={'watching'} 
											word={this.state.word} wrongGuesses={this.state.wrongGuesses} 
											usedLetters={this.state.usedLetters}/>;
				break;
			case 'playing':
				currentView = <HangmanView ws={this.ws} gameState={'playing'} 
											word={this.state.word} wrongGuesses={this.state.wrongGuesses} 
											usedLetters={this.state.usedLetters}/>;
				break;
			case 'gameOver':
				currentView = <GameOver ws={this.ws} p={this.state.p} winner={this.state.winner}
									 	 word={this.state.word} wrongGuesses={this.state.wrongGuesses} />;
				break;
			default:
				break;
		}
		
		return (
			<View>
				{currentView}
			</View>
		);
	}


}

const styles = StyleSheet.create({
  background: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    flex: 1,
    backgroundColor: '#FF6',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
    marginTop: 10,
  },
});


module.exports = hangman;


