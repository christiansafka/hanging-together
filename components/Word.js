'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
} from 'react-native';

import Button from 'apsl-react-native-button'

class Word extends Component 
{
	constructor(props)
	{
		super(props);
	}

	render()
	{
		const checkWordLength = () => {
			if(this.props.word.length <= 8)
			{
				return 34;
			}
			else if(this.props.word.length > 8 && this.props.word.length <= 10)
			{
				return 27;
			}
			else if(this.props.word.length > 10 && this.props.word.length <= 12) { return 22; } 
			else { return 15; }
		}

		let fontsize = checkWordLength();

		const getStyle = () => {
			return {
				color: 'black',
			    fontFamily: 'Avenir',
			    fontSize: fontsize,
			}
		}

		let textSize = getStyle();


		const renderWord = this.props.word.map((letter, i)  => {
			if(letter != ' ')
			{
				return (
					<View key={i} style={styles.letterView}>
						<Text style={textSize}>{letter}</Text>
					</View>
				);
			}
		    return (
				<View key={i} style={styles.letterView}>
					<Text style={textSize}>    </Text>
				</View>
			);
		});

		return (
			<View style={styles.background}>
				{renderWord}
			</View>
		);

	}
}

const styles = StyleSheet.create({
  background: {
  	flexDirection: 'row',
  	flexWrap: 'wrap',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
  },
  letterView: {
  	justifyContent: 'center',
    alignItems: 'center',
    marginRight: 7,
    borderBottomWidth: 3,
    borderBottomColor: 'black',
    
  },
  textStyle: {
    color: 'black',
    fontFamily: 'Avenir',
    fontSize: 35,
  },
});


module.exports = Word;