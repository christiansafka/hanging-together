'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
} from 'react-native';

let Hangman = require('./hangman');


class chooseRoom extends Component {

  constructor(props)
  {
    super(props);
    this.state = {
      text: "",
      gameStart: false,
    }
  }

  render() {

    const startGame = () => {
        this.setState({gameStart: true});
    }

    let roomSelect = 
       <View style={styles.container}>
            <Text style={styles.welcome}>
              Welcome to Hangman!
            </Text>
            <TextInput
              style={{height: 40, width: 200, borderColor: 'gray', borderWidth: 1}}
              onChangeText={(text) => this.setState({text})}
              value={this.state.text}
              placeholder="Enter a room key"
            />
            <TouchableHighlight onPress={startGame}>
              <Text style={styles.buttonText}> Join or Create room </Text>
            </TouchableHighlight>
          </View>;

    let gameStarted =
      <Hangman roomID={this.state.text}></Hangman>;

    let isStarted = (this.state.gameStart) ? gameStarted : roomSelect;

    //the render
    return (
      {isStarted}
    );

  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  buttonText: {
    flex: 1,
    backgroundColor: '#FF6',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
    marginTop: 10,
  },
});

module.exports = chooseRoom;