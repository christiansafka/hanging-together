'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
} from 'react-native';

const Letters = require('./Letters');
const Word = require('./Word');

class HangmanView extends Component 
{
	constructor(props)
	{
		super(props);

		this.letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

		this.makeMessage = this.makeMessage.bind(this);
		this.sendLetter = this.sendLetter.bind(this);

		this.state = {
			usedLetters: this.props.usedLetters,
		}

		this.props.usedLetters.forEach((letter) => {
			if(this.state.usedLetters.indexOf(letter) == -1)
			{
				this.state.usedLetters.push(letter);
				this.setState({usedLetters: this.state.usedLetters});
			}
		});

	}

	makeMessage(action, data)
	{
		let resp = {
            action: action,
            data: data
        };

        return JSON.stringify(resp);
       
	}

	sendLetter(i)
	{
		if(this.props.gameState == 'playing')
		{
			let letter = this.letters[i];
			this.state.usedLetters.push(letter);
			console.log(this.state.usedLetters);
			this.setState({usedLetters: this.state.usedLetters});

			this.props.ws.send(this.makeMessage('sendLetter', letter)); 
		}
		else 
		{
			//do nothing if you are spectating
		}
	}

	render()
	{
		let watching = (this.props.gameState == 'playing') ? 'Playing' : 'Spectating';

		const image0 = require('../images/Hangman0.png');
		const image1 = require('../images/Hangman1.png');
		const image2 = require('../images/Hangman2.png');
		const image3 = require('../images/Hangman3.png');   //could probably do this dynamically
		const image4 = require('../images/Hangman4.png');
		const image5 = require('../images/Hangman5.png');
		const image6 = require('../images/Hangman6.png');
		const images = [image0, image1, image2, image3, image4, image5, image6];
		let currentImage = images[this.props.wrongGuesses];

		return (
			<View style={styles.background}>
				<Image source={currentImage} style={styles.hangmanPic}></Image>
				<Letters sendLetter={this.sendLetter} usedLetters={this.state.usedLetters} />
				<View style={styles.spectating}>
					<Text style={{fontSize: 16}}>{watching}</Text>
				</View>
				<Word word={this.props.word} usedLetters={this.state.usedLetters} />
			</View>
		);
	}

}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  spectating: {
  	width: 350, 
  	height: 25, 
  	borderColor: '#724411', 
  	borderTopWidth: 2, 
  	borderBottomWidth: 2,
  	flexDirection: 'column',
  	justifyContent: 'center',
  	alignItems: 'center',
  },
  hangmanPic: {
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});

module.exports = HangmanView;