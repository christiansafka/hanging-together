'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
} from 'react-native';

import Button from 'apsl-react-native-button'

class Letters extends Component 
{
	constructor(props)
	{
		super(props);
		this.state = {
			letters: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
		}
		//this.renderLetters = this.renderLetters.bind(this);
	}



	
	render()
	{
		const renderLetters = this.state.letters.map((letter, i)  => {
			if(this.props.usedLetters.indexOf(letter) !== -1)
			{
				console.log('disabled letter: ' + letter);
				return <Button style={styles.button} isDisabled={true} textStyle={styles.textStyle} key={i} onPress={this.props.sendLetter.bind(this, i)}>{letter}</Button>;
			}
		    return <Button style={styles.button} textStyle={styles.textStyle} key={i} onPress={this.props.sendLetter.bind(this, i)}>{letter}</Button>;
		});

		return (
			<View style={styles.background}>
				{renderLetters}
			</View>
		);	
	}
}

const styles = StyleSheet.create({
  background: {
  	flexDirection: 'row',
  	flexWrap: 'wrap',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 200,
    marginTop: 25,
  },
  button: {
  	 borderColor: '#2980b9',
     backgroundColor: '#3498db',
     width: 50,
     height: 30,
     marginRight: 4,
  },
  textStyle: {
    color: 'white',
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 26,
  },
});

module.exports = Letters;
